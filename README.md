##part 1.

#we imported all the necessaary tools.


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random
from random import randint



#we opened the databank with the csv reader.


df1 = pd.read_csv(r"/Users/hinnekensxavier/Downloads/Coffeebar_2013-2017.csv",delimiter=";")
print(df1)

#we used the command value_counts() to find the diffrent drinks and foods.


print(df1['DRINKS'].value_counts())
print(df1['FOOD'].value_counts())
print(df1['CUSTOMER'].value_counts())
time = df1["TIME"]
print (time)

#we made the two plots of the sales on the period.


y =[68623, 32010, 31852, 31785]
x = ('sandwich', 'pie', 'cookie', 'muffin')
y_pos = np.arange(len(x))
plt.bar(y_pos, y, color="red")
plt.xticks(y_pos, x)
plt.ylabel('nbr of sales')
plt.title('Total amount of sold food')

plt.show()

y = [95583, 53833, 40915, 40752, 40556, 40436]
x = ('soda', 'coffee', 'water', 'milkshake', 'tea', 'frappucino')
y_pos = np.arange(len(x))
plt.bar(y_pos, y, color="red")
plt.xticks(y_pos, x)
plt.ylabel('nbr of sales')
plt.title('Total amount of sold drinks')

plt.show()


#split of the variable 'TEMPS' in two new ones 'HOUR' and 'DAY'.


df1['DAY'], df1['HOUR'] = df1['TIME'].str.split(' ', 1).str


#group the sales by hour and by drinks/food.


nbdrinksbyhour = df1.groupby(['HOUR', 'DRINKS'])['TIME'].count()
nbfoodbyhour = df1.groupby(['HOUR', 'FOOD'])['TIME'].count()



#we used the following command to find the number of days in the databank.


print(df1['HOUR'].value_counts())

#then we divided our variables nbdrinksbyhour and nbfoodbyhour by the number of day and we multiplied it by 100 to get the probability.


probadrinks = (nbdrinksbyhour / 1825)*100
probafood = (nbfoodbyhour / 1825)*100
print(probadrinks)
print(probafood)


##part 2.


#we created some usefull lists. 


list_customer_id = []

drinklist = ("frappucino", "soda", "tea", "water", "milkshake", "coffee")
drinks_prices = {"frappucino": 4, "soda": 3, "tea": 3, "water": 2, "milkshake": 5, "coffee": 3}

foodlist = ("sandwich", "pie", "cookie", "muffin","")
food_prices = {"": 0, "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3}


#then we created the diffrent customer's profiles.

class Customer(object):
    def __init__(self, customer_id, budget):
        self.customer_id = customer_id
        self.budget = budget

        if Customer in Onetime_customer:
                self.budget = 100
        else:
            if Returning_customer in Hipster:
                self.budget = 500
            else:
                self.budget = 250

#awe added the budget condtion.

    def budgetstillok(self):
        if self.budget <= 10:
            return False
        else:
            return True
            
#we added also the probabilities found in part 1.

    def Giveprobatobuyfood(self, probatobuyfood):
        self.probatobuyfood = probatobuyfood
        self.probatobuyfood = (nbfoodbyhour / 1825) * 100

    def Giveprobatobuydrinks(self, probatobuydrinks):
        self.probatobuydrinks = probatobuydrinks
        self.probatobuydrinks = (nbdrinksbyhour / 1825)*100
        
#we created a function determining the tip randomly between 1 and 10.

    def Givetip(self, tip):
        self.tip = tip
        if onetime_customer in tripadvisor:
            self.tip = randint(1, 10)
        else:
            self.tip = 0
            
#then we created the hierarchy between the classes.

class Onetime_customer(Customer):
    pass


class Tripadvisor(Onetime_customer):
    pass


class Returning_customer(Customer):
    pass


class Hipster(Returning_customer):
    pass



#We created a function picking a number randomly between 0 and 100 and we added some conditions to give the wheigted repartition between the classes

randomnumbers = random.randint(0, 100)
print(randomnumbers)
def selectionoftype():
        if randomnumbers <= 79:
            Customer in Onetime_customer
            if randomnumbers <= 7 >= 0:
                Onetime_customer in Tripadvisor
            else:
                return False

        else:
            Customer in Returning_customer
            if randomnumbers <= 87 >= 80:
                Returning_customer in Hipster
            else:
                return False
                
#we created a function creating a new random id of 8 caracters long and which is added to the list_cutomer_id if it isn't yet into it.

random_id = random.randint(10000000, 99999999)
def generate_random_id():
    while random_id <= 99999999 >= 10000000:
        return "CID", random_id
    else:
        return False


customer_id = generate_random_id()
new_id = list_customer_id.append(customer_id)


def assign_id():
    while customer_id not in list_customer_id:
        return new_id
    else:
        return False

